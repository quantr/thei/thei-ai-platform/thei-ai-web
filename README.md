# THEi AI Platform

Web Portal

# How to start development

1. download Java SE 22.0.1 https://www.oracle.com/java/technologies/
1. download netbeans https://netbeans.apache.org/front/main/download/index.html
1. run netbeans, open the project. click menu team -> git -> clone, enter https://gitlab.com/quantr/thei/thei-ai-platform/thei-ai-web.git

## Deployment

```
mvn -DskipTests clean package
mvn azure-webapp:deploy
```

Refer to https://learn.microsoft.com/en-us/azure/app-service/quickstart-java?tabs=springboot&pivots=java-maven-javase

browse to https://thei1.azurewebsites.net
