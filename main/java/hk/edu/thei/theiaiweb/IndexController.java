package hk.edu.thei.theiaiweb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Controller
public class IndexController {

    @GetMapping("/")
    public String index(ModelMap model, @RequestParam(name = "username", required = false, defaultValue = "") String username, @RequestParam(name = "password", required = false, defaultValue = "") String password) {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement();) {
            QUERY = "SELECT * FROM michael.login;";
            ResultSet rs = stmt.executeQuery(QUERY);
            while (rs.next()) {
                if (rs.getString("username").equals(username) && rs.getString("password").equals(password)) {
                    return "index";
                }
            }
            if (!username.equals("") || !password.equals("")) {
                model.addAttribute("error", "Incorrect username or password");
            }
        } catch (SQLException e) {
        }
        return "login";
    }

    @GetMapping("/michael")
    public String michael(ModelMap model) {
        return "michael";
    }

    @GetMapping("/result")
    public String result(ModelMap model, @RequestParam(name = "name", required = false, defaultValue = "World") String name) {
        System.out.println("index " + name);
        model.put("name", name);
        return "result";
    }

    @GetMapping("/result2")
    public void result2(ModelMap model, @RequestParam(name = "name", required = false, defaultValue = "World") Integer name, @RequestParam(name = "name2", required = false, defaultValue = "World") Integer name2) {
        int r = name + name2;
        System.out.println(r);
        model.put("name", r);
    }

    @GetMapping("/result3")
    public void result3(ModelMap model, @RequestParam(name = "start", required = false, defaultValue = "2") Integer start, @RequestParam(name = "end", required = false, defaultValue = "1") Integer end) {
        String result = "";
        for (int i = start; i <= end; i++) {
            result += Integer.toString(i);
            result += " ";
            System.out.println(result);
        }
        model.put("result", result);
    }

    static final String DB_URL = "jdbc:mysql://localhost/michael";
    static final String USER = "root";
    static final String PASS = "";
    String QUERY = "SELECT * FROM michael.student WHERE id >= 1 && id <= 3;";

    @GetMapping("/result5")
    public void result5(ModelMap model, @RequestParam(name = "name", required = false, defaultValue = "asdsad") String name) {
        String result = "";
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement();) {
            // Extract data from result set
            String update = "INSERT INTO student (`name`) VALUES ('" + name + "');";
            int execute = stmt.executeUpdate(update);
            QUERY = "SELECT * FROM michael.student;";
            ResultSet rs = stmt.executeQuery(QUERY);
            while (rs.next()) {
                // Retrieve by column name
                result += "ID: " + rs.getInt("id");
                result += ", Name: " + rs.getString("name");
                result += " ";
            }
            model.put("result", result);
        } catch (SQLException e) {
            model.put("result", result);
            e.printStackTrace();
        }
    }

    @GetMapping("/signup")
    public String signup(ModelMap model, @RequestParam(name = "username", required = false, defaultValue = "") String username,
            @RequestParam(name = "password", required = false, defaultValue = "") String password,
            @RequestParam(name = "confirm", required = false, defaultValue = "") String confirm) {
        try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS); Statement stmt = conn.createStatement();) {
            String update = "";
            if (password.equals(confirm) && !password.equals("")) {
                update = "INSERT INTO `michael`.`login` (`username`, `password`) VALUES ('" + username + "', '" + password + "');";
                int execute = stmt.executeUpdate(update);
                return "login";
            } else if (!password.equals("")) {
                model.addAttribute("error", "Passwords do not match");
            }
        } catch (SQLException e) {
        }
        return "signup";
    }

}
