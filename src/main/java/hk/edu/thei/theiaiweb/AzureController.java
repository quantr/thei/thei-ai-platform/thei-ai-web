package hk.edu.thei.theiaiweb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/azure")
public class AzureController {

	@GetMapping(value = {"", "/"})
	public String index(ModelMap model) {
		model.put("include1", "azure");
		model.put("include2", "mainBody1");
		return "index";
	}
}
