package hk.edu.thei.theiaiweb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

	@GetMapping("/")
	public String index(ModelMap model, @RequestParam(name = "name", required = false, defaultValue = "World") String name) {
		System.out.println("index " + name);
		model.put("name", name);
		return "index";
	}
}
